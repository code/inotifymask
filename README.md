inotifymask
===========

This tool sets up Linux kernel `inotify` hooks on a set of directories to apply
a bit mask to their permissions on file creation within or movement into the
watched directories, logging each permission change.  This can be used as a way
to enforce permissions for uncooperative applications that don't allow
specifying permissions or masking.

Install
-------

You need Perl v5.10.1 or newer.

    $ cpanm Const::Fast Linux::Inotify2
    $ perl Makefile.PL
    $ make
    $ sudo make install

Use
---

    $ inotifymask 0177 ~/.cache/private ~/.cache/private/templates

Works well as a user-level systemd service; see included `inotifymask.service`
template.

    $ mkdir -p ~/.config/systemd/user
    $ cp inotifymask.service ~/.config/systemd/user
    $ vi ~/.config/systemd/user/inotifymask.service  # Change command line to specify your paths
    $ systemctl --user start inotifymask.service
    $ systemctl --user status inotifymask.service
    $ systemctl --user enable inotifymask.service

License
-------

Copyright (c) [Tom Ryder][1]. Distributed under [MIT License][2].

[1]: https://sanctum.geek.nz/
[2]: https://opensource.org/licenses/MIT
